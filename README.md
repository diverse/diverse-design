# What is Diverse Linux

Diverse Linux is a spin of [Exherbo Linux](https://exherbo.org).
It provides a set of opinionated defaults, lightweight and/or performant
alternatives, binary packages and pre-installed utilities.

From people not accustomed with source based distribution, Diverse Linux can
be considered a mixed distribution (See below).

## Features

- Based on [Musl](https://musl.libc.org/)
- Compiled with LLVM/Clang and aggresive optimizations (`-O3`, `polly` and
  `LTO`)
- [tt](https://github.com/danyspin97/tt) as init/RC manager
- Uses more performant alternatives where possible (e.g.
  [pigz](https://www.zlib.net/pigz/)) and lightweight otherwise (e.g. toybox)
- Comes packed with lightweight programs to improve the system
- Improves security and privacy (e.g. use DNS-over-TLS by default)
- Provides static linked executables
- Installed size reduced by using [UPX](https://upx.github.io/)
- Avaiable in different flavors

## Design Goals

- Test all software on this uncommon configuration and assert that 100% tests
  pass
- Upstream all the changes to both Exherbo and software upstreams

For a detailed list of alternatives choosed see below.

## A mixed distribution

From [Wikipedia](https://en.wikipedia.org/wiki/Gentoo_Linux), the explanation of a **source based distribution** is:

> Unlike a binary software distribution, the source code is compiled locally according to the user's preferences and is often optimized for the specific type of computer. Precompiled binaries are available for some larger packages or those with no available source code.

However, source based distribution needs a lot of tinkering and are time
consuming. The best trade-off is to provide a set of options and distribute
compiled binaries on top of a source based distribution. This is Diverse Linux's
approach.

Exherbo is the source based distribution which Diverse is built upon. It is
a great distribution with **high** quality packages and the devs are open to
substantial upstream changes, like adding tt support.

## Drop-in replacements

### Libraries
- [x] compiler-rt (over libgcc)
- [x] libc++ (over libstdc++)
- [ ] [gettext-tiny](https://github.com/sabotage-linux/gettext-tiny) (over GNU gettext)
- [ ] [netbsd-curses](https://github.com/sabotage-linux/netbsd-curses) (over ncurses)
- [ ] [editline](http://thrysoee.dk/editline/) (over GNU readline)
- [ ] [tinyalsa](https://github.com/tinyalsa/tinyalsa) (over libalsa)

### System Programs
- [ ] [Oilshell](https://www.oilshell.org/) (over bash). Some program do not work with osh (bash interpreter replacement) but there is work ongoing to fix upstream repo. Paludis bash scripts (which are ~16000 LOC as of 2020) still needs to be tested.
- [x] [pigz](https://www.zlib.net/pigz/) (over gzip)
- [ ] [Toybox](https://landley.net/toybox/about.html) (over GNU binutils). Since it does not yet implement all binutils, the remaning ones will taken from [Busybox](https://www.busybox.net/)
- [ ] Linux-ck with Esync patch applied (over Stock Linux kernel)
- [x] [PipeWire](https://pipewire.org/) (over pulseaudio)

## Alternatives
- [x] connman (over NetworkManager). Requires [connman-gtk](https://github.com/jgke/connman-gtk) to be used properly
- [ ] [Nuitka](https://github.com/Nuitka/Nuitka) (over CPython). This could offer a noticeble speed up over CPython. [Benchmark](http://speedcenter.nuitka.net/). Courrent caveits: it needs to compile python packages, so setup-py.exlib should be reworked.

## Utilities

- [ ] [autocpu-freq](https://github.com/AdnanHodzic/auto-cpufreq) when support to desktop is added
- [ ] [benice](https://git.sr.ht/~danyspin97/benice) to optimize responsiveness using CPU and I/O schedulers
- [x] [irqbalance](https://github.com/Irqbalance/irqbalance) to improve irq handlers and cpu load balance
- [x] [gcompat](https://code.foxkit.us/adelie/gcompat) for running executables linked to glibc
- [ ] [hawck](https://github.com/snyball/Hawck) for setting key-bindings
- [ ] [nohang](https://github.com/hakavlad/nohang)
- [x] swayidle: Idle features
- [ ]: [thermald](https://01.org/linux-thermal-daemon/documentation/introduction-thermal-daemon)
- [ ] [tinysnitch](https://github.com/nathants/tinysnitch) as firewall
- [ ] TLP/TLP-Ui to optimize battery life of a laptop
- [x] [unbound](https://github.com/NLnetLabs/unbound) as a lightweight DNS cache

## GUIs

- [ ] [ANGRYsearch](https://github.com/DoTheEvo/ANGRYsearch) as a file search utility
- [ ] [azote](https://github.com/nwg-piotr/azote): Manage screen wallpapers
- [ ] [flameshot](https://github.com/lupoDharkael/flameshot): Screenshot software

- [ ] [flashfocus](https://github.com/fennerm/flashfocus): Add simple focus animations
- [ ] [linux_notification_center](https://github.com/phuhl/linux_notification_center) to manage notifications
- [x] mako: Desktop notifications
- [ ] [swaylock-effects](https://github.com/mortie/swaylock-effects): Lock the screen with awesome effects
- [ ] [swappy](https://github.com/jtheoof/swappy): Snapshot and editor tool
- [ ] [sgtk-menu](https://github.com/nwg-piotr/sgtk-menu): Collections of menu related utilities
- [ ] [Ulauncher](https://github.com/Ulauncher/Ulauncher) as application launcher (can it be used as file search utilily?)

## Configuration
- [ ] [wdisplay](https://github.com/cyclopsian/wdisplays): Manage displays

## Packages distribution
- [ ] [lizard](https://github.com/inikep/lizard) as compression algorithm
- [ ] [upx](https://upx.github.io/): Compress all executables and libraries on the system and decompress them in-place. Offer 40% average compression and the decompression time is unoticeble.

## Privacy
- [ ] [kloak](https://github.com/vmonaco/kloak) for Keystroke-level online anonymization
- [ ] [hblock](https://github.com/hectorm/hblock) to add hosts based blocker
- [x] [stubby](https://github.com/getdnsapi/stubby) for DNS-Over-TLS

## Security

Prioritize security where it does not cause a noticeble performance degradation.

### Compile-time

- [LLVM Scudo Hardened Allocator](https://www.llvm.org/docs/ScudoHardenedAllocator.html): Protect against heap based vulterabilites. TODO: Benchmark the overhead.
- [LLVM SafeStack](https://clang.llvm.org/docs/SafeStack.html): Protect against stack based vulterabilites. Claims that the overhead is less than 0.1% on average.
- [LLVM Control Flow Integrity](https://clang.llvm.org/docs/ControlFlowIntegrity.html): Protect against control flow based attacks. Needs LTO enabled, may not support shared libraries.

### Runtime

- AppArmor
- Firejail

## Gaming utilities

- [ ] [sc-controller](https://github.com/kozec/sc-controller) for configuring controllers
- [x] Gamemode to optimize cpu state when a game starts
- [ ] [MangoHud](https://github.com/flightlessmango/MangoHud) for monitoring PC stats while playing
- [ ] [piper](https://github.com/libratbag/piper) to configure gaming mice

## Flavors
Diverse Linux comes in 3 flavors:
- Desktop with a fully configured Sway window manager
- Desktop with a fully configured Wayfire window manager (and used as DE)
- Mobile with configured phosh for the Pinephone/Librem 5

Both will have full X and Wayland support, with the possibility to remove X11
entirely without issues.

Mobile version is an exception because it doesn't come with X11 installed.

## Development

Diverse Linux is still in a Work In Progress and there is no binary
distribution of the packages yet. However, a lot of work is already done
in Exherbo.

The following is a _todo_ list ordered by priority:

- [ ] Implement tt support
- [ ] Create website
- [ ] Handle Linux kernel as a package in Paludis
- [ ] Add support for static binaries in Exherbo packages
- [ ] Improve binary packages distribution of Paludis
- [ ] Create a reproducible bootstrap method
- [ ] Add fakeroot support to Paludis

